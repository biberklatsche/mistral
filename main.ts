import { app, BrowserWindow, screen, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';
import {ElectronChanel, TerminalChanel} from './src/app/providers/chanels';
const pty =  require('node-pty-prebuilt');

let win, serve;
const args = process.argv.slice(1);
const terminalProcesses = new Map<string, any>();
serve = args.some(val => val === '--serve');

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;
  // Create the browser window.
  win = new BrowserWindow({
    center: true,
    width: size.height / 2 * 1.61 + (serve ? 500 : 0),
    height: size.height / 2,
    minWidth: 600,
    minHeight: 372,
    frame: false
  });

  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200');
    win.openDevTools();
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  ipcMain.on(ElectronChanel.Close, function (evt) {
    app.quit();
  });

  ipcMain.on(TerminalChanel.Exit, (data, arg: {terminalId: string}) => {
    terminalProcesses.delete(arg.terminalId);
  });

  ipcMain.on(TerminalChanel.Startup, (event, arg: {terminalId: string, file: string, columns: number, rows: number}) => {
    const process = pty.spawn(arg.file, [], {
      cols: arg.columns,
      rows: arg.rows
    });
    terminalProcesses.set(arg.terminalId, process);
    process.on('data', (data) => {
      if (process.isSilent) {
        win.webContents.send(TerminalChanel.Silent, {terminalId: arg.terminalId, data: data});
      } else {
        win.webContents.send(TerminalChanel.Data, {terminalId: arg.terminalId, data: data});
      }
    });
  });
  ipcMain.on(TerminalChanel.Data, (event, arg: {terminalId: string, data: string}) => {
    const process = terminalProcesses.get(arg.terminalId);
    process.isSilent = false;
    process.write(arg.data);
  });
  ipcMain.on(TerminalChanel.Silent, (event, arg: {terminalId: string, data: string}) => {
    const process = terminalProcesses.get(arg.terminalId);
    process.isSilent = true;
    process.write(arg.data);
  });
  ipcMain.on(TerminalChanel.Resize, (event, arg: {terminalId: string, columns: number, rows: number}) => {
    const process = terminalProcesses.get(arg.terminalId);
    process.resize(arg.columns, arg.rows);
  });
}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  console.log(e);
  // Catch Error
  // throw e;
}
