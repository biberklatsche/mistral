@echo off
setlocal
set npm_config_disturl="https://atom.io/download/electron"
set npm_config_arch=x64
set npm_config_target_arch=x64
set npm_config_target=2.0.10
set npm_config_runtime="electron"
set npm_config_cache=~\.npm-electron
npm i
endlocal
