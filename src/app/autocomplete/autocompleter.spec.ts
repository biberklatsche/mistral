import {Autocompleter} from './autocompleter';
import {SupportedShell} from '../models/models';
import IpcRenderer = Electron.IpcRenderer;

describe('Autocompleter', () => {

  it('should be created', () => {
    const ipc = new IpcRendererMock();
    expect(new Autocompleter(ipc, '', SupportedShell.WindowsGit)).toBeTruthy();
  });
});

class IpcRendererMock implements IpcRenderer {
  on(channel: string, listener: Function): this {
    return this;
  }
  once(channel: string, listener: Function): this {
    throw new Error('Method not implemented.');
  }
  removeAllListeners(channel: string): this {
    throw new Error('Method not implemented.');
  }
  removeListener(channel: string, listener: Function): this {
    throw new Error('Method not implemented.');
  }
  send(channel: string, ...args: any[]): void {
    throw new Error('Method not implemented.');
  }
  sendSync(channel: string, ...args: any[]) {
    throw new Error('Method not implemented.');
  }
  sendTo(windowId: number, channel: string, ...args: any[]): void {
    throw new Error('Method not implemented.');
  }
  sendToHost(channel: string, ...args: any[]): void {
    throw new Error('Method not implemented.');
  }
  addListener(event: string, listener: Function): this {
    throw new Error('Method not implemented.');
  }
  setMaxListeners(n: number): this {
    throw new Error('Method not implemented.');
  }
  getMaxListeners(): number {
    throw new Error('Method not implemented.');
  }
  listeners(event: string): Function[] {
    throw new Error('Method not implemented.');
  }
  emit(event: string, ...args: any[]): boolean {
    throw new Error('Method not implemented.');
  }
  listenerCount(type: string): number {
    throw new Error('Method not implemented.');
  }
  prependListener(event: string, listener: Function): this {
    throw new Error('Method not implemented.');
  }
  prependOnceListener(event: string, listener: Function): this {
    throw new Error('Method not implemented.');
  }
  eventNames(): string[] {
    throw new Error('Method not implemented.');
  }


}
