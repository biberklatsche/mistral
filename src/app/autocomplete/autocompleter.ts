import {DefaultSuggester, ISuggester} from './suggester/suggester';
import {IAutocompleterData, ISuggestion, Shell} from '../models/models';
import {CdSuggester} from './suggester/cd.suggester';
import IpcRenderer = Electron.IpcRenderer;
import {Ansi} from '../helper/helper';
import {TerminalChanel} from '../providers/chanels';
import {BehaviorSubject, Observable, timer} from 'rxjs';
import {debounce} from 'rxjs/operators';
import { timeout } from 'q';

export class Autocompleter {

  private readonly _terminalId: string;
  private _inputBuffer = '';
  private _promptCharacter: string;
  private _shell: Shell;
  private readonly _ipc: IpcRenderer;
  private _suggesters: Array<ISuggester> = [];
  private readonly _suggestions: BehaviorSubject<ISuggestion[]> = new BehaviorSubject([]);
  private readonly _input: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(ipc: IpcRenderer, terminalId: string, shell: Shell) {
    this._ipc = ipc;
    this._shell = shell;
    this._terminalId = terminalId;
    this._ipc.on(TerminalChanel.Data, (event, arg) => {
      this.extractCurrentPromptCharacter(arg.data);
      const input = this.extractCurrentInput(arg.data);
      this._input.next(input);
    });
    this._input.pipe(
      debounce(() => timer(500))
    ).subscribe((input: string) => {
      const data: IAutocompleterData = {input: input};
      this.suggest(data).subscribe(r => {
        this._suggestions.next(r);
      });
    });
  }

  addSuggester(suggester: ISuggester) {
    suggester.setRunMethode((data) => this.runSilent(data));
    this._suggesters.push(suggester);
  }

  removeSuggester(autocompleter: ISuggester | string) {
    const name = (autocompleter as ISuggester).name ? (autocompleter as ISuggester).name : autocompleter;
    const toRemove = this._suggesters.find(a => a.name === name);
    const index = this._suggesters.indexOf(toRemove);
    this._suggesters.splice(index, 1);
  }

  suggestionsAsObservable(): Observable<ISuggestion[]> {
    return this._suggestions.asObservable();
  }

  runSilent(command: string): Observable<string> {
    const inputWithoutPrompt = this._inputBuffer.replace(this._promptCharacter + ' ', '');
    const deleteCurrent = this._shell.characters.backspace.repeat(inputWithoutPrompt.length);
    const enter = this._shell.characters.enter;
    const silentCommand = deleteCurrent + command + enter + inputWithoutPrompt;
    const result = Observable.create((observer) => {
      this._ipc.on(TerminalChanel.Silent, (event, arg) => {
        if (Ansi.removeControlChars(arg.data).indexOf('/') !== -1) {
          observer.next(Ansi.removeControlChars(arg.data));
        } else if (Ansi.removeControlChars(arg.data).endsWith(inputWithoutPrompt)) {
          this._ipc.removeAllListeners(TerminalChanel.Silent);
          observer.complete();
        }
      });
    });
    this._ipc.send(TerminalChanel.Silent, {terminalId: this._terminalId, data: deleteCurrent + command + enter + inputWithoutPrompt});
    return result;
  }

  private suggest(data: IAutocompleterData): Observable<ISuggestion[]> {
    const suggester = this.findSuggester(data);
    return suggester.suggest(data);
  }

  private findSuggester(context): ISuggester {
    const suggesters = [...this._suggesters].filter(a => a.match(context) > 0);
    return suggesters.sort((a, b) => b.match(context) >= a.match(context) ? -1 : 1)[0];
  }

  private extractCurrentInput(data: string): string {
    if (!data) {
      return '';
    }
    const inputLine = Ansi.removeControlChars(data);
    if (Ansi.containsPrompt(inputLine, this._promptCharacter)) {
      this._inputBuffer = Ansi.removePrompt(inputLine, this._promptCharacter);
    } else if (data === '[K') {
      this._inputBuffer = this._inputBuffer.substring(0, this._inputBuffer.length - 1);
    } else {
      this._inputBuffer += inputLine;
    }
    return this._inputBuffer;
  }

  private extractCurrentPromptCharacter(data: string) {
    const prompt = Ansi.getPromptCharacterFromEmptyInput(data);
    if (prompt) {
      this._promptCharacter = prompt;
    }
    return this._promptCharacter;
  }
}

export namespace AutocompleterFactory {
  export function create(ipc: IpcRenderer, terminalId: string, shell: Shell): Autocompleter {
    const autocompleter = new Autocompleter(ipc, terminalId, shell);
    const cdSuggester = new CdSuggester();
    cdSuggester.setRunMethode(this.runSilent);
    if (cdSuggester.matchShell(shell.family)) {
      autocompleter.addSuggester(cdSuggester);
    }
    const defaultSuggester = new DefaultSuggester();
    if (defaultSuggester.matchShell(shell.family)) {
      autocompleter.addSuggester(defaultSuggester);
    }
    return autocompleter;
  }
}
