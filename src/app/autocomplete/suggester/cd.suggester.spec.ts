
import {CdSuggester} from './cd.suggester';
import {IAutocompleterData, SupportedShell} from '../../models/models';

describe('CdSuggester', () => {

  it('should be created', () => {
    expect(new CdSuggester()).toBeTruthy();
  });

  it('empty context should return empty list', () => {
    const cdSuggester = new CdSuggester();
    cdSuggester.setRunMethode((command: string) => '');
    expect(cdSuggester.suggest(null)).toEqual([]);
  });

  it('should match cd-autocompleter', () => {
    const cdSuggester = new CdSuggester();
    cdSuggester.setRunMethode((command: string) => 'Hello World.');
    const context: IAutocompleterData = {input: 'cd '};
    expect(cdSuggester.suggest(context)[0]).toEqual({command: 'Hello World.', label: 'Hello World.', description: ''});
  });
});
