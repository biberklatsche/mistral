import {ISuggester} from './suggester';
import {IAutocompleterData, ISuggestion, ShellFamily} from '../../models/models';
import { CodegenComponentFactoryResolver } from '@angular/core/src/linker/component_factory_resolver';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export class CdSuggester implements ISuggester {
  readonly name = 'CdSuggester';
  readonly regexp = new RegExp('.*\\s?cd $', 'gm');

  private run: (command: string) => Observable<string>;

  matchShell(shellfamily: ShellFamily): boolean {
    if (ShellFamily.Unix === shellfamily) {
      return true;
    }
    return false;
  }

  match(context: IAutocompleterData): number {
    if (this.regexp.test(context.input)) {
      return 1.0;
    }
    return 0.0;
  }

  suggest(context: IAutocompleterData): Observable<ISuggestion[]> {
    if (!context) {
      return of([]);
    }
    return this.run('ls -d */').pipe(
      switchMap(result => {
        const directories = result.replace('\\r', '').replace('\\n', '').split('\\t');
        console.log(directories);
        const r = new Array();
        for (const directory of directories) {
          r.push({command: directory, label: directory, description: ''});
        }
        return r;
      })
    );
  }

  setRunMethode(run: (command: string) => Observable<string>) {
    this.run = run;
  }
}
