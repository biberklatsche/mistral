import {IAutocompleterData, ISuggestion, ShellFamily} from '../../models/models';
import { Observable, of } from 'rxjs';

export interface ISuggester {
  name: string;
  match(context: IAutocompleterData): number;
  matchShell(shellfamily: ShellFamily): boolean;
  suggest(context: IAutocompleterData): Observable<ISuggestion[]>;
  setRunMethode(run: (command: string) => Observable<string>);
}

export class DefaultSuggester implements ISuggester {
  name = 'Empty';

  constructor() {

  }

  match(context: IAutocompleterData): number {
    return 0.0001;
  }

  suggest(context: IAutocompleterData): Observable<ISuggestion[]> {
    return of([]);
  }

  setRunMethode(run: (command: string) => Observable<string>) {

  }

  matchShell(shellfamily: ShellFamily): boolean {
    return true;
  }
}
