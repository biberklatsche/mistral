import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ISuggestion} from '../../models/models';
import {switchMap} from 'rxjs/operators';
import {TabService} from '../../providers/tab.service';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  @Input() terminalId: Observable<string>;

  autocomplete: Observable<ISuggestion[]>;

  constructor(private readonly tabService: TabService) { }

  ngOnInit() {
    this.autocomplete = this.terminalId.pipe(
      switchMap(terminalId => this.tabService.autocomplete(terminalId))
    );
  }

}
