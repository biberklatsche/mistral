import {Component, OnDestroy, OnInit} from '@angular/core';
import {ElectronService} from '../../providers/electron.service';
import {fromEvent, interval, Subscription, timer} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public isMouseMoved = true;
  private isMouseMovedSubscription: Subscription;

  constructor(private readonly electron: ElectronService) { }

  ngOnInit() {
    timer(5000).subscribe(() => this.isMouseMoved = false);
    this.isMouseMovedSubscription = fromEvent(document, 'mousemove').pipe(
      tap(val => this.isMouseMoved = true),
      switchMap(val => interval(5000))
    ).subscribe( val => this.isMouseMoved = false);
  }


  close(){
    this.electron.close();
  }

  ngOnDestroy(): void {
    this.isMouseMovedSubscription.unsubscribe();
  }

}
