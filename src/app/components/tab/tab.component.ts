import { Component, OnInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  terminalId: BehaviorSubject<string> = new BehaviorSubject('');

  constructor() { }

  ngOnInit() {
  }

  onTerminalId(id){
    this.terminalId.next(id);
  }

}
