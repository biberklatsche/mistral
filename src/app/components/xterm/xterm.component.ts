import {Component, EventEmitter, HostListener, OnDestroy, OnInit, Output} from '@angular/core';
import * as xterm from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import {TabService} from '../../providers/tab.service';
import {Observable, Subscription} from 'rxjs';
import {ISuggestion, SupportedShell} from '../../models/models';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-xterm',
  templateUrl: './xterm.component.html',
  styleUrls: ['./xterm.component.scss']
})
export class XTermComponent implements OnInit, OnDestroy {

  private terminal: xterm.Terminal;
  private outputSubscription: Subscription;
  private _terminalId: string;
  @Output() terminalId = new EventEmitter<string>();
  suggestions: Observable<ISuggestion[]>;
  constructor(
    private readonly tabService: TabService) { }

  ngOnInit() {
    xterm.Terminal.applyAddon(fit);
    this.terminal = new xterm.Terminal({
      fontFamily: 'Roboto Mono',
      cursorBlink: true,
      cursorStyle: 'underline',
      theme: {
        background: 'rgb(241,241,241)',
        cursor: 'rgb(6, 187, 249)',
        foreground: 'rgb(95, 95, 95)',
        selection: 'rgba(6, 187, 249, 0.5)'}
    });
    this.terminal.open(document.getElementById('terminal'));
    (this.terminal as any).fit();
    const rows = this.terminal.rows;
    const cols = this.terminal.cols;
    const terminalId = this.tabService.startup(SupportedShell.MacBash, cols, rows);
    this.terminalId.emit(terminalId);
    this._terminalId = terminalId;
    this.terminal.on('data', (data) => {
      this.tabService.input(this._terminalId, data);
    });
    this.outputSubscription = this.tabService.output(this._terminalId).subscribe(data => {
      this.terminal.write(data);
    });
    this.terminal.focus();

    this.suggestions = this.tabService.autocomplete(this._terminalId).pipe(
      tap(s => console.log('Autocomplete: ', s))
    );
  }

  ngOnDestroy(): void {
    if (this.outputSubscription) {
      this.outputSubscription.unsubscribe();
    }
    this.tabService.remove(this._terminalId);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    (this.terminal as any).fit();
    const rows = this.terminal.rows;
    const cols = this.terminal.cols;
    this.tabService.resize(this._terminalId, cols, rows);
  }
}
