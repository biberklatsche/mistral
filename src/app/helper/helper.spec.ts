import {Ansi} from './helper';

describe('Ansi', () => {

  it('null should return null', () => {
    expect(Ansi.removeControlChars(null)).toBeNull();
  });

  it('empty should return empty', () => {
    expect(Ansi.removeControlChars('')).toBe('');
  });

  it('remove1', () => {
    expect(Ansi.removeControlChars('[2H')).toBe('');
  });

  it('remove2', () => {
    expect(Ansi.removeControlChars('[0K')).toBe('');
  });

  it('remove3', () => {
    expect(Ansi.removeControlChars('[0K[3G')).toBe('');
  });

  it('remove4', () => {
    expect(Ansi.removeControlChars(']0;')).toBe('');
  });

  it('remove5', () => {
    expect(Ansi.removeControlChars('[?25l')).toBe('');
  });

  it('removeBell', () => {
    expect(Ansi.removeControlChars('')).toBe('');
  });

  it('containsPrompt1', () => {
    expect(Ansi.containsPrompt('$', '$')).toBeTruthy();
  });

  it('containsPrompt2', () => {
    console.log('test');
    expect(Ansi.containsPrompt('sad$ ls', '$')).toBeTruthy();
  });

  it('containsPrompt3', () => {
    expect(Ansi.containsPrompt(' hasdf as', '$')).toBeFalsy();
  });

  it('containsPrompt4', () => {
    expect(Ansi.containsPrompt(' ', '$')).toBeFalsy();
  });

  it('containsPrompt4', () => {
    expect(Ansi.containsPrompt('bash-3.2$ ', '$')).toBeTruthy();
  });

  it('getPromptCharacterFromEmptyInput_gitbash', () => {
    // tslint:disable-next-line:max-line-length
    expect(Ansi.getPromptCharacterFromEmptyInput(']0;MINGW64:/c/Projects/mistral[?25l\n[0;32mLars.Wolfram@BART [0;35mMINGW64 [0;33m/c/Projects/mistral[0;36m (master)[0m[0K\n$[0K[3G[?25h')).toBe('$');
  });

  it('getPromptCharacterFromEmptyInput_macterminal', () => {
    expect(Ansi.getPromptCharacterFromEmptyInput('[?1034hbash-3.2$')).toBe('$');
  });

  it('removePropmt1', () => {
    expect(Ansi.removePrompt('$', '$')).toBe('');
  });

  it('removePropmt2', () => {
    expect(Ansi.removePrompt('abc-123$', '$')).toBe('');
  });

  it('removePropmt2', () => {
    expect(Ansi.removePrompt('123-.abc$ test', '$')).toBe('test');
  });
});
