

export namespace Guid {
  export function newGuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}

export namespace Ansi {
  const escapeChars = ['$'];

  const removeControlCharsPattern = [
    '[\x9B\x1B]\\[[0-?]*[ -/]*[@-~]',
    '\x07',
    '[\x9B\x1B]\\][0-9];',
    '\x1B\\[\\?1034h'
  ].join('|');

  const containsPromptPattern = '[a-zA-Z0-9\\[\\-\\:\\.\\\\\\s]*';
  const getPromptCharacterPattern = '[a-zA-Z0-9\\[\\-\\:\\.\\\\\\s]*([$>]{1})';

  export function removeControlChars(s: string) {
    if (!s) {
      return s;
    }
    if (s === '[6G') {
      return ' ';
    }
    const result = s.replace(new RegExp(removeControlCharsPattern, 'g'), '');
    return result;
  }

  export function containsPrompt(s: string, promptCharacter: string) {
    if (escapeChars.find(escape => escape === promptCharacter)) {
      promptCharacter = '\\' +  promptCharacter;
    }
    return new RegExp(containsPromptPattern + promptCharacter, 'gm').test(s);
  }

  export function getPromptCharacterFromEmptyInput(s: string): string {
    const text = this.removeControlChars(s);
    const result = new RegExp(getPromptCharacterPattern, 'gm').exec(text);
    return result && result[1] ? result[1].replace(new RegExp('[\r\n\\s]?', 'g'), '') : null;
  }

  export function removePrompt(s: string, promptCharacter: string) {
    const indexOfPropmt = s.indexOf(promptCharacter) + 1;
    return s.substring(indexOfPropmt, s.length).trim();
  }
}

