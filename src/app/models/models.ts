export interface ISuggestion {
  label: string;
  description: string;
  command: string;
}

export interface IAutocompleterData {
  readonly input: string;
}

export interface Shell {
  file: string;
  family: ShellFamily;
  os: OS;
  characters: {
    backspace: string;
    enter: string;
  };
}

export enum ShellFamily {
  Unix
}

export enum OS {
  Windows, Mac, Unsupported
}

export namespace SupportedShell {
  export const WindowsGit: Shell = {
    file: 'C:\\Arbeit\\Git\\bin\\sh.exe',
    family: ShellFamily.Unix,
    os: OS.Windows,
    characters: {
      backspace: '\x08',
      enter: ''
    }
  };

  export const WindowsBash: Shell = {
    file: 'C:\\Windows\\System32\\bash.exe',
    family: ShellFamily.Unix,
    os: OS.Windows,
    characters: {
      backspace: '\x08',
      enter: ''
    }
  };

  export const MacBash: Shell = {
    file: '/bin/bash',
    family: ShellFamily.Unix,
    os: OS.Mac,
    characters: {
      backspace: String.fromCharCode(21),
      enter: '\x0a'
    }
  };
}

