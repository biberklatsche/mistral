export namespace TerminalChanel {
  export const Data = "data";
  export const Silent = "silent";
  export const Startup = "startup";
  export const Resize = "resize";
  export const Exit = "exit";
}

export namespace ElectronChanel {
  export const Close = "close";
}

