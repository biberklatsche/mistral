import {Injectable} from '@angular/core';
import {ElectronService} from './electron.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {TerminalChanel} from './chanels';
import {ISuggestion, Shell} from '../models/models';
import IpcRenderer = Electron.IpcRenderer;
import {Autocompleter, AutocompleterFactory} from '../autocomplete/autocompleter';
import {Guid} from '../helper/helper';

@Injectable({
  providedIn: 'root'
})
export class TabService {

  private _tabContexts: Map<string, TabContext> = new Map();

  constructor(private readonly electron: ElectronService) {

  }

  autocomplete(terminalId: string): Observable<ISuggestion[]> {
    return this._tabContexts.get(terminalId).autocompleteAsObservable();
  }

  /**
   * pty-process (terminal backend) -> xterm (terminal frontend)
   * @param terminalId
   */
  output(terminalId: string): Observable<string> {
    return this._tabContexts.get(terminalId).outputAsObservable();
  }

  /**
   * xterm (terminal frontend) -> pty-process (terminal backend)
   * @param terminalId
   * @param data
   */
  input(terminalId: string, data: string) {
    const context = this._tabContexts.get(terminalId);
    context.handleTerminalInput(data);
  }

  startup(shell: Shell, columns: number, rows: number): string {
    const context = new TabContext(this.electron.ipcRenderer, shell);
    this._tabContexts.set(context.terminalId, context);
    context.start(columns, rows);
    return context.terminalId;
  }

  resize(terminalId: string, columns: number, rows: number) {
    const context = this._tabContexts.get(terminalId);
    context.resize(columns, rows);
  }

  remove(terminalId: string) {
    const context = this._tabContexts.get(terminalId);
    context.exit();
  }
}

class TabContext {
  public readonly terminalId: string;
  private readonly _terminal: TerminalConnector;
  private readonly _autocompleter: Autocompleter;

  constructor(ipc: IpcRenderer, shell: Shell) {
    this.terminalId = Guid.newGuid();
    this._terminal = new TerminalConnector(ipc, this.terminalId, shell);
    this._autocompleter = AutocompleterFactory.create(ipc, this.terminalId, shell);
  }

  handleTerminalInput(data: string) {
    this._terminal.writeToInput(data);
  }

  autocompleteAsObservable(): Observable<ISuggestion[]> {
    return this._autocompleter.suggestionsAsObservable();
  }

  outputAsObservable(): Observable<string> {
    return this._terminal.outputAsObservable();
  }

  start(columns: number, rows: number) {
    this._terminal.start(columns, rows);
  }

  resize(columns: number, rows: number) {
    this._terminal.resize(columns, rows);
  }

  exit() {
    this._terminal.exit();
  }
}

export class TerminalConnector {
  private readonly _terminalId: string;
  private readonly _shell: Shell;
  private readonly _output: BehaviorSubject<string>;
  private readonly _ipc: IpcRenderer;

  constructor(ipc: IpcRenderer, terminalId: string, shell: Shell) {
    this._output = new BehaviorSubject<string>('');
    this._ipc = ipc;
    this._terminalId = terminalId;
    this._shell = shell;
    this._ipc.on(TerminalChanel.Data, (event, arg) => {
      this.writeToOutput(arg.data);
    });
  }

  outputAsObservable(): Observable<string> {
    return this._output.asObservable();
  }

  writeToInput(data: string) {
    this._ipc.send(TerminalChanel.Data, {terminalId: this._terminalId, data: data});
  }

  resize(columns: number, rows: number) {
    this._ipc.send(TerminalChanel.Resize, {terminalId: this._terminalId, columns: columns, rows: rows});
  }

  exit() {
    this._ipc.send(TerminalChanel.Exit, {terminalId: this._terminalId});
  }

  start(columns: number, rows: number) {
    this._ipc.send(TerminalChanel.Startup, {terminalId: this._terminalId, file: this._shell.file, columns: columns, rows: rows});
  }

  private writeToOutput(data: string) {
    this._output.next(data);
  }
}
